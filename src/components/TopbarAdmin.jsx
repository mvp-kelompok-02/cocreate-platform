import React, {useState, useEffect }  from "react";
import { useDispatch, useSelector } from 'react-redux';
import { logout } from "../actions/UserActions";

const Topbar = ({ history }) => {
    const dispatch = useDispatch();

    const logoutHandler = () => {
        dispatch(logout());
        window.location = "/login";
    }

    return (
        <nav className="navbar navbar-expand-lg main-navbar px-5">
            <a href="index.html" className="navbar-brand">CoCreate</a>
            <form className="form-inline ml-auto">
                <ul className="navbar-nav">
                    <li><a href="#" data-toggle="search" className="nav-link nav-link-lg d-none"><i className="fas fa-search" /></a></li>
                </ul>
            </form>
            <ul className="navbar-nav navbar-right">
                <li className="dropdown">
                    <a href="#" className="nav-link nav-link-lg nav-link-user">
                        <img alt="image" src="../assets/img/avatar/avatar-1.png" className="rounded-circle" style={{ border:"2px solid white" }} />
                    </a>
                </li>
                <li className="dropdown dropdown-list-toggle">
                    <a type="submit" onClick={logoutHandler} class="btn btn-outline-light text-light">Logout</a>
                </li>
            </ul>
        </nav>
    )
}

export default Topbar