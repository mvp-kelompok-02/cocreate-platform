import React from "react";

function SidebarAdmin() {
    return (
        <div className="main-sidebar">
            <aside id="sidebar-wrapper">
                <ul className="sidebar-menu">
                    <li><a className="nav-link" href="dashboard"><i className="fas fa-fire" style={{ fontSize:"16px" }} /> <span>Dashboard</span></a></li>
                    <li><a className="nav-link" href="kelola-registrasi"><i className="far fa-file-alt" style={{ fontSize:"16px" }} /> <span>Registrasi</span></a></li>
                    <li><a className="nav-link" href="blank.html"><i className="far fa-newspaper" style={{ fontSize:"16px" }} /> <span>Artikel</span></a></li>
                    <li><a className="nav-link" href="blank.html"><i className="fas fa-tasks" style={{ fontSize:"16px" }} /> <span>Kategori</span></a></li>
                    <li><a className="nav-link" href="blank.html"><i className="fas fa-users-cog" style={{ fontSize:"16px" }} /> <span>Pengaturan Akun</span></a></li>
                </ul>
            </aside>
        </div>
    )
}

export default SidebarAdmin