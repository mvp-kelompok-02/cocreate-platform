import React from "react";

function Footer() {
    return (
        <footer className="main-footer">
            <div className="footer-left">
                Copyright © 2020 <div className="bullet" /> CoCreate Platform
            </div>
        </footer>
    )
}

export default Footer;