import React from "react";

function Sidebar() {
    return (
        <div className="main-sidebar" style={{ position: "sticky", top: "32px" }}>
            <aside id="sidebar-wrapper">
                <ul className="sidebar-menu d-none d-md-block">
                    {/* <li><a className="nav-link" href="blank.html"><i className="fas fa-home pr-4" style={{ fontSize:"20px" }} /> <span style={{ fontSize:"18px", fontWeight:"initial" }}>Beranda</span></a></li>
                            <li><a className="nav-link" href="blank.html"><i className="fas fa-users pr-4" style={{ fontSize:"20px" }} /> <span style={{ fontSize:"18px", fontWeight:"initial" }}>Grup</span></a></li>
                            <li><a className="nav-link" href="credits.html"><i className="fas fa-pencil-alt pr-4" style={{ fontSize:"20px" }} /> <span style={{ fontSize:"18px", fontWeight:"initial" }}>Tulis artikel</span></a></li> */}

                    <a href="/home" class="btn btn-icon btn-primary p-4" style={{ width: "120px" }}>
                        <i className="fas fa-home mb-2" style={{ fontSize: "22px" }}></i><br />
                        <h5 style={{ fontSize: "16px", fontWeight: "initial" }}>Beranda</h5>
                    </a>
                    <a href="/grup" class="btn btn-icon btn-primary p-4 my-4" style={{ width: "120px" }}>
                        <i className="fas fa-users mb-2" style={{ fontSize: "22px" }}></i><br />
                        <h5 style={{ fontSize: "16px", fontWeight: "initial" }}>Grup</h5>
                    </a>
                    <a href="/submit-artikel" class="btn btn-icon btn-primary p-4" style={{ width: "120px" }}>
                        <i className="fas fa-pencil-alt mb-2" style={{ fontSize: "22px" }}></i><br />
                        <h5 style={{ fontSize: "16px", fontWeight: "initial" }}>Buat <br /> artikel</h5>
                    </a>
                </ul>
                {/* <div className="mt-4 mb-4 p-3 hide-sidebar-mini">
                    <a href="https://getstisla.com/docs" className="btn btn-primary btn-lg btn-block btn-icon-split">
                        <i className="fas fa-rocket" /> Documentation
                    </a>
                </div> */}
            </aside>
        </div>
    );
}

export default Sidebar;