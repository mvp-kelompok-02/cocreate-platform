import './App.css';

import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
} from 'react-router-dom';
import { useSelector } from 'react-redux';

import ProtectedRoute from './pages/ProtectedRoute';
import Registrasi from './pages/auth/Registrasi';
import Verifikasi from './pages/auth/Verifikasi';
import Login from './pages/auth/Login';
import PilihKategori from './pages/anggota/PilihKategori';
import Home from './pages/anggota/Home';
import Profil from './pages/anggota/profil/Profil';
import Grup from './pages/anggota/Grup';
import Artikel from './pages/anggota/Article';
import DetailArtikel from './pages/anggota/DetailArtikel';
import BuatArtikel from './pages/anggota/BuatArtikel';

import Dashboard from './pages/admin/DashboardAdmin';
import KelolaRegistrasi from './pages/admin/OlahRegistrasi';

const App = () => {
    const userLogin = useSelector((state) => state.userLogin);
    const { token } = userLogin;

    return (
        <Router>
            <Switch>
                <Route exact path="/daftar" component={Registrasi} />
                <Route exact path="/verifikasi" component={Verifikasi}></Route>
                <Route exact path="/">
                    <Redirect to="/login" />
                </Route>
                <Route exact path="/login" component={Login}></Route>
                <Route exact path="/welcome" component={PilihKategori}></Route>
                <Route exact path="/home" component={Home}></Route>
                <Route exact path="/profil" component={Profil}></Route>
                <Route exact path="/grup" component={Grup}></Route>
                <Route exact path="/artikel" component={Artikel}></Route>
                <Route exact path="/detail-artikel" component={DetailArtikel}></Route>
                <Route exact path="/submit-artikel" component={BuatArtikel}></Route>

                <Route exact path="/dashboard" component={Dashboard}></Route>
                <Route exact path="/kelola-registrasi" component={KelolaRegistrasi}></Route>
            </Switch>
        </Router>
    );
};

export default App;
