import {
    USER_REGISTER_FAIL,
    USER_REGISTER_REQUEST,
    USER_REGISTER_SUCCESS,
    USER_REGISTER_STATUS_RESET,
    USER_LOGIN_FAIL,
    USER_LOGIN_REQUEST,
    USER_LOGIN_SUCCESS,
    USER_DATA_REQUEST,
    USER_DATA_SUCCESS,
    USER_DATA_FAIL,
    USER_DATA_RESET,
    USER_LOGOUT,
  } from "../constants/UserConstants";
  
  export const userLoginReducer = (state = {}, action) => {
    switch (action.type) {
      case USER_LOGIN_REQUEST:
        return { loading: true };
      case USER_LOGIN_SUCCESS:
        return { loading: false, token: action.payload };
      case USER_LOGIN_FAIL:
        return { loading: false, error: action.payload };
      default:
        return state;
    }
  };

  export const userDataReducer = (state = { user: {} }, action) => {
    switch (action.type) {
      case USER_DATA_REQUEST:
        return {
          loading: true,
        }
      case USER_DATA_SUCCESS:
        return {
          loading: false,
          user: action.payload,
        }
      case USER_DATA_FAIL:
        return {
          loading: false,
          error: action.payload,
        }
      default:
        return state
    }
  };