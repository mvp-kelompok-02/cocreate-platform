import React, { Component } from 'react';
import axios from 'axios';
import Topbar from '../../components/TopbarAdmin';
import Sidebar from '../../components/SidebarAdmin';
import Footer from '../../components/Footer';

class OlahRegistrasi extends Component {
    constructor(props) {
        super(props)

        this.state = {
            users: []
        }
    }

    componentDidMount() {
        axios.get('http://54.214.195.229:4121/account/all',
            {
                headers: {
                    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXIxQGdtYWlsLmNvbSIsInJvbGUiOjEsInVzZXJfaWQiOjI4fQ.3FfhcTKWoUQ5w-8TwBfA1GPpq8nDpl3XegOuvuGk-Ho'
                }
            })
            .then(response => {
                console.log(response.data.data.account)
                this.setState({ users: response.data.data.account })
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        const { users } = this.state
        return (
            <div id="app">
                <div className="main-wrapper">
                    <div className="navbar-bg" />

                    <Topbar />
                    <Sidebar />

                    {/* Main Content */}
                    <div className="main-content">
                        <section className="section">
                            <div className="section-header">
                                <h1>Olah Registrasi</h1>
                            </div>
                        </section>
                        <div className="row">
                            <div className="col-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>Daftar Registrasi Anggota</h4>
                                        <div className="card-header-form">
                                            <form>
                                                <div className="input-group">
                                                    <a href="#" class="btn btn-light">Tolak registrasi</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div className="card-body p-0">
                                        {
                                            users.length ?
                                                <div className="table-responsive">
                                                    <table className="table table-striped">
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <div className="custom-checkbox custom-control">
                                                                        <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" className="custom-control-input" id="checkbox-all" />
                                                                        <label htmlFor="checkbox-all" className="custom-control-label">&nbsp;</label>
                                                                    </div>
                                                                </th>
                                                                <th>Nama lengkap</th>
                                                                <th>email</th>
                                                                <th>Tanggal daftar</th>
                                                                <th>Status</th>
                                                            </tr>
                                                            {users.map(user => (
                                                                <tr key={user.user_id}>
                                                                    <td className="p-0 text-center" key={user.user_id} width="50px">
                                                                        <div className="custom-checkbox custom-control">
                                                                            <input type="checkbox" data-checkboxes="mygroup" className="custom-control-input" id={user.user_id} />
                                                                            <label htmlFor={user.user_id} className="custom-control-label">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                    <td key={user.user_id}>{user.first_name} {user.last_name}</td>
                                                                    <td key={user.user_id}>{user.email}</td>
                                                                    <td key={user.user_id}>{user.created_at}</td>
                                                                    <td key={user.user_id}>
                                                                        {
                                                                            user.is_verified === 1 ? 
                                                                            <div class="badge badge-success">Verified</div> : <div class="badge badge-danger">Not verified</div>
                                                                        }
                                                                    </td>
                                                                </tr>
                                                            ))}
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            : null
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default OlahRegistrasi