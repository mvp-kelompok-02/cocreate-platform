import React, { useState, Fragment } from "react";
import Topbar from '../../components/TopbarAdmin';
import Sidebar from '../../components/SidebarAdmin';
import Footer from '../../components/Footer';

function DashboardAdmin() {
    return (
      <Fragment>
        <div id="app">
          <div className="main-wrapper">
            <div className="navbar-bg" />

            <Topbar />
            <Sidebar />

            {/* Main Content */}
            <div className="main-content">
              <section className="section">
                <div className="section-header">
                  <h1>Dashboard</h1>
                </div>
                <div className="row">
                  <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div className="card card-statistic-1">
                      <div className="card-icon bg-success">
                        <i className="far fa-file" />
                      </div>
                      <div className="card-wrap">
                        <div className="card-header">
                          <h4>Registrasi</h4>
                        </div>
                        <div className="card-body">
                          7
                </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div className="card card-statistic-1">
                      <div className="card-icon bg-info">
                        <i className="fas fa-user-edit" />
                      </div>
                      <div className="card-wrap">
                        <div className="card-header">
                          <h4>Kontributor</h4>
                        </div>
                        <div className="card-body">
                          5
                </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div className="card card-statistic-1">
                      <div className="card-icon bg-danger">
                        <i className="far fa-newspaper" />
                      </div>
                      <div className="card-wrap">
                        <div className="card-header">
                          <h4>Artikel</h4>
                        </div>
                        <div className="card-body">
                          13
                </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div className="card card-statistic-1">
                      <div className="card-icon bg-warning">
                        <i className="fas fa-users" />
                      </div>
                      <div className="card-wrap">
                        <div className="card-header">
                          <h4>Grup Proyek Inovasi</h4>
                        </div>
                        <div className="card-body">
                          4
                </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div className="card card-statistic-1">
                      <div className="card-icon bg-primary">
                        <i className="fas fa-user-shield" />
                      </div>
                      <div className="card-wrap">
                        <div className="card-header">
                          <h4>Admin</h4>
                        </div>
                        <div className="card-body">
                          2
                </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
            <Footer />
          </div>
        </div>

      </Fragment>
    )
}

export default DashboardAdmin