import React, { useState } from "react";
import Topbar from "../../components/Topbar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";

function BuatArtikel() {
    const postHandler = () => {
        alert("Artikel berhasil di-post")
    }

    return (
        <div id="app">
            <div className="main-wrapper">
                <div className="navbar-bg" />
                <Topbar />
                <Sidebar />

                {/* Main Content */}
                <div className="main-content">
                    <section className="section">
                        <div className="section-header">
                            <h1>Buat Artikel</h1>
                            <div className="section-header-breadcrumb">
                                <a href="/artikel" className="btn btn-primary">Daftar artikel saya</a>
                            </div>
                        </div>
                        <div className="section-body">
                            <h2 className="section-title">Buat artikel baru</h2>
                            <p className="section-lead">
                                Anda bisa membuat dan mem-posting artikel melalui form berikut
                            </p>
                            <div className="row">
                                <div className="col-12">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4>Tulis Artikel</h4>
                                        </div>
                                        <div className="card-body">
                                            <div className="form-group row mb-4">
                                                <label className="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul</label>
                                                <div className="col-sm-12 col-md-7">
                                                    <input type="text" className="form-control" />
                                                </div>
                                            </div>
                                            <div className="form-group row mb-4">
                                                <label className="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kategori</label>
                                                <div className="col-sm-12 col-md-7">
                                                    <select className="form-control selectric">
                                                        <option>Teknologi</option>
                                                        <option>Keuangan</option>
                                                        <option>Gaya hidup</option>
                                                        <option>Kuliner</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-group row mb-4">
                                                <label className="col-form-label text-md-right col-12 col-md-3 col-lg-3">Isi artikel</label>
                                                <div className="col-sm-12 col-md-7">
                                                    <textarea className="form-control summernote-simple" style={{ height:"150px" }} defaultValue={""} />
                                                </div>
                                            </div>
                                            <div className="form-group row mb-4">
                                                <label className="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar feature</label>
                                                <div className="col-sm-12 col-md-7">
                                                    <div class="custom-file">
                                                        <input type="file" className="custom-file-input" id="customFile" />
                                                        <label className="custom-file-label" for="customFile">Pilih file</label>
                                                    </div>
                                                    {/* <div id="image-preview" className="image-preview">
                                                        <label htmlFor="image-upload" id="image-label">Pilih file</label>
                                                        <input type="file" name="image" id="image-upload" />
                                                    </div> */}
                                                </div>
                                            </div>
                                            <div className="form-group row mb-4">
                                                <label className="col-form-label text-md-right col-12 col-md-3 col-lg-3" />
                                                <div className="col-sm-12 col-md-7">
                                                    <button className="btn btn-primary" onClick={postHandler}>Post artikel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <Footer />
            </div>
        </div>
    )
}

export default BuatArtikel