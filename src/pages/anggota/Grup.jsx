import React, { useState } from "react";
import Topbar from "../../components/Topbar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";

function Grup() {
  return (
    <div id="app">
        <div className="main-wrapper">
            <div className="navbar-bg" />

                <Topbar />
                <Sidebar />

                {/* Main Content */}
                <div className="main-content">
                  <section className="section">
                      <div className="section-header">
                            <h1>Grup</h1>
                            <div className="section-header-breadcrumb">
                                <a href="#" className="btn btn-primary">Buat grup baru</a>
                            </div>
                      </div>
                      <div className="section-body">
                          <h2 className="section-title">Daftar grup Proyek Inovasi (PI)</h2>
                          <p className="section-lead">
                              Daftar grup PI yang anda buat/ikuti
                            </p>
                          <div className="row">
                              <div className="col-lg-6">
                                  <div className="card card-large-icons">
                                      <div className="card-icon bg-light text-primary">
                                          <i className="fas fa-users" />
                                      </div>
                                      <div className="card-body">
                                          <h4>Grup 1</h4>
                                          <p>Di bagian ini akan ditampilkan deskripsi singkat terkait grup ini</p>
                                          <a href="#" className="card-cta">Kunjungi grup <i className="fas fa-chevron-right" /></a>
                                      </div>
                                  </div>
                              </div>
                              <div className="col-lg-6">
                                  <div className="card card-large-icons">
                                      <div className="card-icon bg-light text-primary">
                                          <i className="fas fa-users" />
                                      </div>
                                      <div className="card-body">
                                          <h4>Grup 2</h4>
                                          <p>Di bagian ini akan ditampilkan deskripsi singkat terkait grup ini</p>
                                          <a href="#" className="card-cta">Kunjungi grup <i className="fas fa-chevron-right" /></a>
                                      </div>
                                  </div>
                              </div>
                              <div className="col-lg-6">
                                  <div className="card card-large-icons">
                                      <div className="card-icon bg-light text-primary">
                                          <i className="fas fa-users" />
                                      </div>
                                      <div className="card-body">
                                          <h4>Grup 3</h4>
                                          <p>Di bagian ini akan ditampilkan deskripsi singkat terkait grup ini</p>
                                          <a href="#" className="card-cta">Kunjungi grup <i className="fas fa-chevron-right" /></a>
                                      </div>
                                  </div>
                              </div>
                              <div className="col-lg-6">
                                  <div className="card card-large-icons">
                                      <div className="card-icon bg-light text-primary">
                                          <i className="fas fa-users" />
                                      </div>
                                      <div className="card-body">
                                          <h4>Grup 4</h4>
                                          <p>Di bagian ini akan ditampilkan deskripsi singkat terkait grup ini</p>
                                          <a href="#" className="card-cta">Kunjungi grup <i className="fas fa-chevron-right" /></a>
                                      </div>
                                  </div>
                              </div>
                              <div className="col-lg-6">
                                  <div className="card card-large-icons">
                                      <div className="card-icon bg-light text-primary">
                                          <i className="fas fa-users" />
                                      </div>
                                      <div className="card-body">
                                          <h4>Grup 5</h4>
                                          <p>Di bagian ini akan ditampilkan deskripsi singkat terkait grup ini</p>
                                          <a href="#" className="card-cta">Kunjungi grup <i className="fas fa-chevron-right" /></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
              </div>

                <Footer />
        </div>
    </div>
  )
}

export default Grup;