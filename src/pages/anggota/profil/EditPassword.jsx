import React, { Component } from 'react';
import axios from 'axios';

class EditPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            old_password: '',
            new_password: '',
            confirm_password: ''
        }
    }

    changeHandler = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    submitHandler = e => {
        e.preventDefault()
        console.log(this.state)
        axios
            .put('http://kelompok2.dtstakelompok1.com/account/password', this.state,
            {
                headers: {
                  'Authorization': localStorage.getItem('token')
                }
            })
            .then(response => {
                this.setState({
                    old_password: '',
                    new_password: '',
                    confirm_password: ''
                });
                console.log(response)
                alert('Password berhasil diubah')
            })
            .catch(error => {
                console.log(error)
                alert('Terjadi kesalahan. Pastikan semua data telah diisi dengan benar')
            })
    }

    render() {
        const { old_password, new_password, confirm_password } = this.state

        return (
            <div className="card">
                <form onSubmit={this.submitHandler}>
                    <div className="card-header">
                        <h4>Ganti Password</h4>
                    </div>
                    <div className="card-body pb-0">
                        <div className="form-group">
                            <label htmlFor="inputPassword5">Password lama</label>
                            <input 
                                type="password"
                                id="inputPassword5"
                                className="form-control"
                                aria-describedby="passwordHelpBlock"
                                name="old_password"
                                value={old_password}
                                onChange={this.changeHandler}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputPassword5">Password baru</label>
                            <input
                                type="password"
                                id="inputPassword5"
                                className="form-control"
                                aria-describedby="passwordHelpBlock"
                                name="new_password"
                                value={new_password}
                                onChange={this.changeHandler}
                            />
                            <small id="passwordHelpBlock" className="form-text text-muted">
                                Password terdiri dari minimal 8 karakter
                        </small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputPassword5">Konfirmasi password baru</label>
                            <input
                                type="password"
                                id="inputPassword5"
                                className="form-control"
                                aria-describedby="passwordHelpBlock"
                                name="confirm_password"
                                value={confirm_password}
                                onChange={this.changeHandler}
                            />
                        </div>
                    </div>
                    <div className="card-footer bg-whitesmoke text-right">
                        <button className="btn btn-primary" type="submit">Perbarui password</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default EditPassword;