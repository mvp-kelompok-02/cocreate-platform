import React, { Component } from 'react';
import axios from 'axios';
import Datetime from 'react-datetime';
import "react-datetime/css/react-datetime.css";
import Topbar from "../../../components/Topbar";
import Sidebar from "../../../components/Sidebar";
import Footer from "../../../components/Footer";
import EditPassword from "./EditPassword";

class Profil extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {}
    }
  }

  componentDidMount() {
    // const { token } = localStorage.getItem('token')
    axios.get('http://kelompok2.dtstakelompok1.com/account',
      {
        headers: {
          'Authorization': localStorage.getItem('token')
        }
      })
      .then(response => {
        console.log(response.data.data.account)
        this.setState({ 
          user: response.data.data.account,
          first_name:  response.data.data.account.first_name,
          last_name: response.data.data.account.last_name,
          email: response.data.data.account.email,
          phone: response.data.data.account.phone,
          bod: response.data.data.account.bod,
          address: response.data.data.account.address,
          image: response.data.data.account.image,
          bio: response.data.data.account.bio
        })
        
      })
      .catch(error => {
        console.log(error)
      })
  }

  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value })
    // const imageToBase64 = require('image-to-base64');
    // imageToBase64(user.image) // Path to the imag
  }

  changeDateHandler = e => {
    this.setState({...this.state, bod: e.format("YYYY-MM-DD")}) 
  }
  

  submitProfilHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios
      .put('http://kelompok2.dtstakelompok1.com/account/edit', this.state,
        {
          headers: {
            'Authorization': localStorage.getItem('token')
          }
        })
      .then(response => {
        console.log(response)
        window.location.reload(false)
        alert('Profil berhasil diperbarui')
      })
      .catch(error => {
        console.log(error)
        alert('Terjadi kesalahan. Pastikan semua data telah diisi dengan benar')
      })
  }
  

  render() {
    const { user, first_name, last_name, email, phone, bod, address, image, bio } = this.state

    return (
      <div id="app">
        <div className="main-wrapper">
          <div className="navbar-bg" />
  
          <Topbar />
          <Sidebar />
  
          {/* Main Content */}
          <div className="main-content">
            <section className="section">
              <div className="section-header">
                <h1>Profil</h1>
              </div>
              <div className="section-body">
                <h2 className="section-title">Selamat pagi, { user.first_name }</h2>
                <p className="section-lead">
                  Silakan lengkapi atau ubah profil Anda pada halaman ini.
                </p>
                <div className="row mt-sm-4">

                  <div className="col-12 col-md-12 col-lg-5">
                    <div className="card profile-widget">
                      <div className="profile-widget-header">
                        <img alt="image" src="../assets/img/avatar/avatar-1.png" className="rounded-circle profile-widget-picture" />
                        <div className="profile-widget-items">
                          <div className="profile-widget-item">
                            <div className="profile-widget-item-label">Artikel yang ditulis</div>
                            <div className="profile-widget-item-value">9</div>
                          </div>
                        </div>
                      </div>
                      <div className="profile-widget-description">
                        <div className="profile-widget-name">{user.first_name} {user.last_name}</div>
                        { user.bio }
                      </div>
                    </div>

                    <EditPassword />
                  </div>

                  <div className="col-12 col-md-12 col-lg-7">
                    <div className="card">
                      <form onSubmit={this.submitProfilHandler}>
                        <div className="card-header">
                          <h4>Edit Profil</h4>
                        </div>
                        <div className="card-body pb-0">
                          <div className="row" hidden>
                            <div className="form-group col-12">
                              <label>ID</label>
                              <input name="id_user" type="text" className="form-control" defaultValue={ user.user_id } />
                            </div>
                          </div>
                          <div className="row">
                            <div className="form-group col-md-6 col-12">
                              <label>Nama depan</label>
                              <input
                                type="text"
                                className="form-control"
                                name="first_name"
                                value={first_name}
                                defaultValue={ user.first_name }
                                onChange={this.changeHandler}
                                required
                              />
                              <div className="invalid-feedback">
                                Please fill in the first name
                              </div>
                            </div>
                            <div className="form-group col-md-6 col-12">
                              <label>Nama belakang</label>
                              <input
                                type="text"
                                className="form-control"
                                name="last_name"
                                defaultValue={ user.last_name }
                                value={last_name}
                                onChange={this.changeHandler}
                                required />
                              <div className="invalid-feedback">
                                Please fill in the last name
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="form-group col-md-7 col-12">
                              <label>Email</label>
                              <input
                                type="email"
                                className="form-control"
                                name="email"
                                defaultValue={ user.email }
                                value={email}
                                onChange={this.changeHandler}
                                required />
                              <div className="invalid-feedback">
                                Please fill in the email
                              </div>
                            </div>
                            <div className="form-group col-md-5 col-12">
                              <label>Nomor telepon</label>
                              <input
                                type="tel"
                                className="form-control"
                                name="phone"
                                value={phone}
                                defaultValue={ user.phone }
                                onChange={this.changeHandler}
                              />
                            </div>
                          </div>
                          <div className="row">
                            <div className="form-group col-12">
                              <label>Tanggal lahir</label>
                              {/* <input
                                type="date"
                                className="form-control"
                                name="bod"
                                value={bod}
                                defaultValue={ dob }
                                onChange={this.changeHandler}
                              /> */}
                              <Datetime 
                                viewMode="days"
                                timeFormat={false}
                                dateFormat="DD-MM-YYYY"
                                initialValue={ bod }
                                value={bod}
                                onChange={this.changeDateHandler}
                              />
                            </div>
                          </div>
                          <div className="row">
                            <div className="form-group col-12">
                              <label>Alamat</label>
                              <textarea
                                className="form-control summernote-simple"
                                style={{ height:"80px" }}
                                name="address"
                                value={address}
                                defaultValue={ user.address }
                                onChange={this.changeHandler}
                              />
                            </div>
                          </div>
                          <div className="row">
                            <div className="form-group col-12">
                              <label>Foto profil</label>
                              <div class="custom-file">
                                <input type="file" className="custom-file-input" id="customFile" />
                                <label className="custom-file-label" for="customFile">Pilih file</label>
                              </div>
                              {/* <div id="image-preview" className="image-preview" style={{ height:"120px", width:"120px" }}>
                                  <label htmlFor="image-upload" id="image-label" style={{ width:"80px" }} >Pilih file</label>
                                  <input
                                    type="file"
                                    name="image"
                                    id="image-upload"
                                    value={image}
                                    defaultValue={ user.image }
                                    onChange={this.changeHandler}
                                  />
                              </div> */}
                            </div>
                          </div>
                          <div className="row">
                            <div className="form-group col-12">
                              <label>Bio</label>
                              <textarea
                                className="form-control summernote-simple"
                                style={{ height:"107px" }}
                                name="bio"
                                value={bio}
                                defaultValue={ user.bio }
                                onChange={this.changeHandler}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="card-footer bg-whitesmoke text-right">
                          <button className="btn btn-primary" type="submit">Perbarui profil</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
  
          <Footer />
        </div>
      </div>
    );
  }
  
}

export default Profil;
