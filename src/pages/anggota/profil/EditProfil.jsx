import React, { Component } from 'react';
import axios from 'axios';

class EditProfil extends Component {
    constructor(props) {
        super(props)

        this.state = {
            user: {}
        }
    }

    componentDidMount() {
        axios
            .get('http://54.214.195.229:4121/account',
            {
                headers: {
                    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXIyQGdtYWlsLmNvbSIsInJvbGUiOjIsInVzZXJfaWQiOjI5fQ.6DBcj5PMMU4Xtp5BCXdqO2ZSVrXDd_tidLqdYnhar4U'
                }
            })
            .then(response => {
                console.log(response.data.data.account)
                this.setState({ user: response.data.data.account })
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        const { user } = this.state

        return (
            <div className="card">
                <form method="post" className="needs-validation" noValidate>
                    <div className="card-header">
                        <h4>Edit Profil</h4>
                    </div>
                    <div className="card-body pb-0">
                        <div className="row" hidden>
                            <div className="form-group col-12">
                                <label>ID</label>
                                <input name="id_user" type="text" className="form-control" defaultValue={user.user_id} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6 col-12">
                                <label>Nama depan</label>
                                <input type="text" className="form-control" defaultValue={user.first_name} required />
                                <div className="invalid-feedback">
                                    Please fill in the first name
                              </div>
                            </div>
                            <div className="form-group col-md-6 col-12">
                                <label>Nama belakang</label>
                                <input type="text" className="form-control" defaultValue={user.last_name} required />
                                <div className="invalid-feedback">
                                    Please fill in the last name
                              </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-7 col-12">
                                <label>Email</label>
                                <input type="email" className="form-control" defaultValue={user.email} required />
                                <div className="invalid-feedback">
                                    Please fill in the email
                              </div>
                            </div>
                            <div className="form-group col-md-5 col-12">
                                <label>Nomor telepon</label>
                                <input type="tel" className="form-control" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-12">
                                <label>Tanggal lahir</label>
                                <input type="date" className="form-control" defaultValue="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-12">
                                <label>Alamat</label>
                                <textarea className="form-control summernote-simple" style={{ height: "80px" }} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-12">
                                <label>Foto profil</label>
                                {/* <div class="custom-file">
                                <input type="file" className="custom-file-input" id="customFile" />
                                <label className="custom-file-label" for="customFile">Pilih file</label>
                              </div> */}
                                <div id="image-preview" className="image-preview" style={{ height: "120px", width: "120px" }}>
                                    <label htmlFor="image-upload" id="image-label" style={{ width: "80px" }} >Pilih file</label>
                                    <input type="file" name="image" id="image-upload" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-12">
                                <label>Bio</label>
                                <textarea className="form-control summernote-simple" style={{ height: "107px" }} defaultValue={"Di bagian ini akan tampil bio user. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sunt ut suscipit soluta deserunt dolor doloribus aliquam dolorem consequatur eligendi quo deleniti vitae voluptatibus ex voluptate quae ratione, laboriosam assumenda modi!"} />
                            </div>
                        </div>
                    </div>
                    <div className="card-footer bg-whitesmoke text-right">
                        <button className="btn btn-primary">Perbarui profil</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default EditProfil;