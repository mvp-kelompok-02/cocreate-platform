import React from "react";

function PilihKategori() {
    return (
        <div id="app">
            <section className="section">
                <div className="container mt-5" style={{ width:"50%" }}>
                    <div className="card">
                        <div className="card-body">
                            <h6>Sebelum anda memulai, silahkan pilih kategori yang ingin anda ikuti</h6>
                            <br/>
                            <form>
                                <div className="form-group" style={{ fontSize:"15px" }}>
                                    <div className="form-check my-2">
                                        <input className="form-check-input" type="checkbox" id="defaultCheck1" />
                                        <label className="form-check-label" for="defaultCheck1">
                                            Keuangan
                                        </label>
                                    </div>
                                    <div className="form-check my-2">
                                        <input className="form-check-input" type="checkbox" id="defaultCheck2" />
                                        <label className="form-check-label" for="defaultCheck2">
                                            Teknologi
                                        </label>
                                    </div>
                                    <div className="form-check my-2">
                                        <input className="form-check-input" type="checkbox" id="defaultCheck3" />
                                        <label className="form-check-label" for="defaultCheck3">
                                            Gaya hidup
                                        </label>
                                    </div>
                                    <div className="form-check my-2">
                                        <input className="form-check-input" type="checkbox" id="defaultCheck4" />
                                        <label className="form-check-label" for="defaultCheck4">
                                            Masak
                                        </label>
                                    </div>
                                    <div className="form-check my-2">
                                        <input className="form-check-input" type="checkbox" id="defaultCheck5" />
                                        <label className="form-check-label" for="defaultCheck5">
                                            Pilih semua
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="card-footer text-right">
                            <a type="submit" href="/home" className="btn btn-primary">Lanjutkan</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default PilihKategori