import React, {useState, useEffect }  from "react";
import { useDispatch, useSelector } from 'react-redux';
import Topbar from "../../components/Topbar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";

//import gambar
// import Idea from '../img/idea.svg';
// import Artikel from '../img/contract.svg';


const Home = ({ history }) => {
    const dispatch = useDispatch();

    const userLogin = useSelector((state) => state.userLogin);
    const { error, token } = userLogin;

    // Validasi, kalau belum login ga bisa akses
    useEffect(() => {
        if (token) {
          history.push("/home");
        } else {
            history.push("/login");
        }
    }, [history, token]);
    
    return (
        <div id="app">
            <div className="main-wrapper">
                <div className="navbar-bg" />
                
                <Topbar />
                <Sidebar />
                
                {/* Main Content */}
                <div className="main-content">
                    <section className="section">
                        <div className="row mt-3">
                            <div className="col-lg-7">
                                <h4 style={{ fontWeight: "initial" }}>Top contributors</h4>
                                <p style={{ fontSize: "16px" }}>Kontributor teraktif, dengan konten yang paling digemari</p>
                            </div>
                            <div className="col-6 col-sm-3 col-lg-1 mb-4 mb-md-0">
                                <div className="avatar-item">
                                    <img alt="image" src="../assets/img/avatar/avatar-1.png" className="img-fluid" width="64px" data-toggle="tooltip" title="Waluyo" />
                                </div>
                            </div>
                            <div className="col-6 col-sm-3 col-lg-1 mb-4 mb-md-0">
                                <div className="avatar-item">
                                    <img alt="image" src="../assets/img/avatar/avatar-2.png" className="img-fluid" width="64px" data-toggle="tooltip" title="Satria" />
                                </div>
                            </div>
                            <div className="col-6 col-sm-3 col-lg-1 mb-4 mb-md-0">
                                <div className="avatar-item">
                                    <img alt="image" src="../assets/img/avatar/avatar-3.png" className="img-fluid" width="64px" data-toggle="tooltip" title="Ningsih" />
                                </div>
                            </div>
                            <div className="col-6 col-sm-3 col-lg-1 mb-4 mb-md-0">
                                <div className="avatar-item">
                                    <img alt="image" src="../assets/img/avatar/avatar-4.png" className="img-fluid" width="64px" data-toggle="tooltip" title="Zubaedah" />
                                </div>
                            </div>
                            <div className="col-6 col-sm-3 col-lg-1 mb-4 mb-md-0">
                                <div className="avatar-item">
                                    <img alt="image" src="../assets/img/avatar/avatar-5.png" className="img-fluid" width="64px" data-toggle="tooltip" title="Kontributor 5" />
                                </div>
                            </div>
                        </div>
                        <hr/><br/>
                        <div className="section-body">
                            <h2 style={{ fontSize:"28px", fontWeight:"initial" }}>CoCreate Feed</h2>
                            <p style={{ fontSize:"16px" }}>Kumpulan artikel pilihan dari kontributor CoCreate</p>
                            
                            <div className="card pt-0">
                                <div className="card-body p-0 m-0 row">
                                    <div className="col-md-5" style={{ height:"300px", backgroundImage: "url(../assets/img/news/img01.jpg)", backgroundSize: "cover", backgroundPosition: "center" }}>
                                    </div>
                                    <div className="col-md-7 py-4 px-5">
                                        <h4><a href="">Judul Artikel 1</a></h4>
                                        <p>Posted by <a href=""><b> Author 1 </b></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 11 November 2020 &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Kategori</p> <br/>
                                        <p style={{ fontWeight:"initial" }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis...
                                        </p>
                                        <div class="text-right">
                                            <a href="#">Selengkapnya <i class="fas fa-chevron-right"></i></a>
                                        </div>
                                        <hr/>
                                        <i className="far fa-heart pr-1" style={{ fontSize:"16px" }} /> <span className="mr-4">12</span>
                                        <i className="far fa-comments pr-1" style={{ fontSize:"16px" }} /> <span>2</span>
                                    </div>
                                </div>
                            </div>

                            <div className="card pt-0">
                                <div className="card-body p-0 m-0 row">
                                    <div className="col-md-5" style={{ height:"300px", backgroundImage: "url(../assets/img/news/img01.jpg)", backgroundSize: "cover", backgroundPosition: "center" }}>
                                    </div>
                                    <div className="col-md-7 py-4 px-5">
                                        <h4><a href="">Judul Artikel 2</a></h4>
                                        <p>Posted by <a href=""><b> Author 2 </b></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 11 November 2020 &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Kategori</p> <br/>
                                        <p style={{ fontWeight:"initial" }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis...
                                        </p>
                                        <div class="text-right">
                                            <a href="#">Selengkapnya <i class="fas fa-chevron-right"></i></a>
                                        </div>
                                        <hr/>
                                        <i className="far fa-heart pr-1" style={{ fontSize:"16px" }} /> <span className="mr-4">12</span>
                                        <i className="far fa-comments pr-1" style={{ fontSize:"16px" }} /> <span>2</span>
                                    </div>
                                </div>
                            </div>

                            <div className="card pt-0">
                                <div className="card-body p-0 m-0 row">
                                    <div className="col-md-5" style={{ height:"300px", backgroundImage: "url(../assets/img/news/img01.jpg)", backgroundSize: "cover", backgroundPosition: "center" }}>
                                    </div>
                                    <div className="col-md-7 py-4 px-5">
                                        <h4><a href="">Judul Artikel 3</a></h4>
                                        <p>Posted by <a href=""><b> Author 3 </b></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 11 November 2020 &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Kategori</p> <br/>
                                        <p style={{ fontWeight:"initial" }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis...
                                        </p>
                                        <div class="text-right">
                                            <a href="#">Selengkapnya <i class="fas fa-chevron-right"></i></a>
                                        </div>
                                        <hr/>
                                        <i className="far fa-heart pr-1" style={{ fontSize:"16px" }} /> <span className="mr-4">12</span>
                                        <i className="far fa-comments pr-1" style={{ fontSize:"16px" }} /> <span>2</span>
                                    </div>
                                </div>
                            </div>

                            <div className="card pt-0">
                                <div className="card-body p-0 m-0 row">
                                    <div className="col-md-5" style={{ height:"300px", backgroundImage: "url(../assets/img/news/img01.jpg)", backgroundSize: "cover", backgroundPosition: "center" }}>
                                    </div>
                                    <div className="col-md-7 py-4 px-5">
                                        <h4><a href="">Judul Artikel 4</a></h4>
                                        <p>Posted by <a href=""><b> Author 4 </b></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 11 November 2020 &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Kategori</p> <br/>
                                        <p style={{ fontWeight:"initial" }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis...
                                        </p>
                                        <div class="text-right">
                                            <a href="#">Selengkapnya <i class="fas fa-chevron-right"></i></a>
                                        </div>
                                        <hr/>
                                        <i className="far fa-heart pr-1" style={{ fontSize:"16px" }} /> <span className="mr-4">12</span>
                                        <i className="far fa-comments pr-1" style={{ fontSize:"16px" }} /> <span>2</span>
                                    </div>
                                </div>
                            </div>

                            <div className="card pt-0">
                                <div className="card-body p-0 m-0 row">
                                    <div className="col-md-5" style={{ height:"300px", backgroundImage: "url(../assets/img/news/img01.jpg)", backgroundSize: "cover", backgroundPosition: "center" }}>
                                    </div>
                                    <div className="col-md-7 py-4 px-5">
                                        <h4><a href="">Judul Artikel 5</a></h4>
                                        <p>Posted by <a href=""><b> Author 5 </b></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 11 November 2020 &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Kategori</p> <br/>
                                        <p style={{ fontWeight:"initial" }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis...
                                        </p>
                                        <div class="text-right">
                                            <a href="#">Selengkapnya <i class="fas fa-chevron-right"></i></a>
                                        </div>
                                        <hr/>
                                        <i className="far fa-heart pr-1" style={{ fontSize:"16px" }} /> <span className="mr-4">12</span>
                                        <i className="far fa-comments pr-1" style={{ fontSize:"16px" }} /> <span>2</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                
                <Footer />
            </div>
        </div>
    );
}

export default Home;
