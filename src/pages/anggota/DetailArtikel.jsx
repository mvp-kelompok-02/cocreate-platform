import React from 'react';
import Topbar from "../../components/Topbar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";

function DetailArtikel() {
    return (
        <div id="app">
            <div className="main-wrapper">
                <div className="navbar-bg" />

                <Topbar />
                <Sidebar />

                {/* Main Content */}
                <div className="main-content">
                    <section className="section">
                        <br />
                        <div className="section-body">
                            <center><h2 style={{ fontSize: "38px", fontFamily: "CASTELLAR", color: "darkblue" }}>Article Title</h2></center><br /><br />

                            <div className="card pt-0">
                                <div className="card-body p-0 m-0 row">
                                    <div className="col-md-5" style={{ height: "500px", backgroundImage: "url(../assets/img/news/img01.jpg)", backgroundSize: "cover", backgroundPosition: "center" }}>
                                    </div>
                                    <div className="col-md-7 py-4 px-5">
                                        {/* <h4><a href="">Judul Artikel 1</a></h4> */}
                                        <p style={{ fontSize: "20px" }}>Posted by <a href=""><b> Author 1 </b></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 11 November 2020 &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Kategori</p> <br />
                                        <p style={{ fontFamily: "Comic Sans", fontSize: "32px", textAlign: "justify" }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.
                                </p>
                                        {/* <div class="text-right">
                                    <a href="/artikel">Selengkapnya <i class="fas fa-chevron-right"></i></a>
                                </div> */}
                                        <br /><hr />
                                        <button id="like" className="btn far fa-heart pr-1" style={{ fontSize: "22px" }} onClick="myfunction(this)" /> <span className="mr-4">12</span>
                                        <i className="far fa-share-square pr-1" style={{ fontSize: "22px" }} /> <span>2</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <br /><br /><br />

                        <div className="card-body">
                            <p style={{ fontSize: "20px" }}>Comment</p><br />
                            <ul className="list-unstyled list-unstyled-border list-unstyled-noborder">
                                <li className="media">
                                    <img alt="image" className="mr-3 rounded-circle" width={70} src="../assets/img/avatar/avatar-1.png" />
                                    <div className="media-body">
                                        <div className="media-right"><a href=""><i className="far fa-heart pr-1" /></a></div>
                                        <div className="media-title mb-1">Rizal Fakhri</div>
                                        <div className="text-time">Yesterday</div>
                                        <div className="media-description text-muted">Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                                        <div className="media-links">
                                            <a href="#">Like</a>
                                            <div className="bullet" />
                                            <a href="#">Reply</a>
                                            {/* <div className="bullet" />
                <a href="#" className="text-danger">Trash</a> */}
                                        </div>
                                    </div>
                                </li>
                                <li className="media">
                                    <img alt="image" className="mr-3 rounded-circle" width={70} src="../assets/img/avatar/avatar-2.png" />
                                    <div className="media-body">
                                        {/* <div className="media-right"><div className="text-warning">Pending</div></div> */}
                                        <div className="media-title mb-1">Bambang Uciha</div>
                                        <div className="text-time">Yesterday</div>
                                        <div className="media-description text-muted">Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                                        <div className="media-links">
                                            <a href="#">Like</a>
                                            <div className="bullet" />
                                            <a href="#">Reply</a>
                                            {/* <div className="bullet" />
                <a href="#" className="text-danger">Trash</a> */}
                                        </div>
                                    </div>
                                </li>
                                <li className="media">
                                    <img alt="image" className="mr-3 rounded-circle" width={70} src="../assets/img/avatar/avatar-3.png" />
                                    <div className="media-body">
                                        {/* <div className="media-right"><div className="text-primary">Approved</div></div> */}
                                        <div className="media-title mb-1">Ujang Maman</div>
                                        <div className="text-time">Yesterday</div>
                                        <div className="media-description text-muted">Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident</div>
                                        <div className="media-links">
                                            <a href="#">Like</a>
                                            <div className="bullet" />
                                            <a href="#">Reply</a>
                                            {/* <div className="bullet" />
                <a href="#" className="text-danger">Trash</a> */}
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>




                <Footer />
            </div>
        </div>
    );
}
export default DetailArtikel;
