import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Image } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import img from '../../img/image.png';
import { login } from "../../actions/UserActions";
import GoogleBtn from "./GoogleBtn";

const Login = ({ history }) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const dispatch = useDispatch();

    const userLogin = useSelector((state) => state.userLogin);
    const { error, token, role } = userLogin;

    useEffect(() => {
        if (token) {
          history.push("/welcome");
        } else {
            history.push("/login");
        }
    }, [history, token]);

    useEffect(() => {
        if (error && error !== undefined) {
            alert('Login gagal. Silakan cek apakah data yang anda masukkan sudah benar');
        }
    }, [error]);
    
    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(login(email, password));
    };

    const registHandler = () => {
        history.push('/daftar');
    }

    return (
        <div className="container py-4">
            <section className="card section">
                <div className="container mx-3 mt-4">
                    <h3 className="text-dark font-weight-normal">Selamat datang di <span className="font-weight-bold">CoCreate Platform</span></h3>
                    <p className="text-muted">Ini tulisan suntuk subtitle nya. Terserah mau diisi apaan</p>
                </div>
                
                <div className="row">
                    <div className="col-lg-6 col-12 order-lg-1 order-1 align-self-center">
                        <div className="p-4 m-3">
                            <Image src={img} width="100%"></Image>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-6 col-12 order-lg-2 order-2">
                        <div className="p-5 m-lg-4">
                            {/* <img src="../assets/img/stisla-fill.svg" alt="logo" width={80} className="shadow-light rounded-circle mb-5 mt-2" /> */}
                            <h4 className="text-dark font-weight-normal mb-5">Login</h4>
                            <form onSubmit={submitHandler}>
                                <div className="form-group">
                                    <label htmlFor="email" className="control-label">Email</label>
                                    <input
                                        id="email"
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        value={email}
                                        onChange={e => setEmail(e.target.value)}
                                        tabIndex={1}
                                        required
                                        autofocus
                                    />
                                    <div className="invalid-feedback">
                                        Mohon isi alamat email anda
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="d-block">
                                        <label htmlFor="password" className="control-label">Password</label>
                                    </div>
                                    <input
                                        id="password"
                                        type="password"
                                        className="form-control"
                                        name="password"
                                        value={password}
                                        onChange={e => setPassword(e.target.value)}
                                        tabIndex={2}
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Mohon isi password anda
                                    </div>
                                </div>
                                <div className="form-group text-right mb-4">
                                    {/* <a href="auth-forgot-password.html" className="float-right mt-3">
                                        Forgot Password?
                                    </a> */}
                                    <button type="submit" className="btn btn-primary btn-lg btn-icon icon-right">
                                        Login
                                    </button>
                                    <span className="float-left mt-3">
                                        Belum punya akun? <a type="submit" onClick={registHandler}>Daftar sekarang</a>
                                    </span>
                                </div>
                                <div><GoogleBtn/></div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </section>
        </div>
    )
}

export default Login