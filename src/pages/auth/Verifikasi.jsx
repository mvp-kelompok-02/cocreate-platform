import React from "react";
import { Image } from "react-bootstrap";
import img from '../../img/image.png';

function Verifikasi() {
    return (
        <div id="app">
            <section className="section">
                <div className="container mt-5">
                    <div className="page-error">
                        <div className="page-inner">
                            <div className="page-description pb-5">
                                <h4>Horeee, Akun Anda Sudah Terdaftar<br/>Silakan Cek Email Untuk Verfikasi yaa</h4>
                            </div>
                            <Image src={img} width="40%"></Image>
                            <br/><br/>
                            <a href="/login" className="btn btn-primary mt-5 px-5">
                                Kembali ke halaman login
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Verifikasi