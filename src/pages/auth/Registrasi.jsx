import React, { Component, Fragment } from "react";
import { Image } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Axios from 'axios';
import { Link } from 'react-router-dom';
import img from '../../img/image.png'


export default class Registrasi extends Component {
    // melakukan registrasi
    handleSubmit(event) {
        event.preventDefault();

        const data = {
            first_name: this.first_name,
            last_name: this.last_name,
            email: this.email,
            password: this.password,
            confirm_password: this.confirm_password,
        };

        // ambil data
        Axios.post('http://kelompok2.dtstakelompok1.com/auth/register', data)
            .then((res) => {
                console.log(res);
                window.location = "/verifikasi" 
            })
            .catch((err) => {
                console.log(err);
                alert('Registrasi gagal. Silakan cek apakah data yang anda masukkan sudah benar');
            });

        console.log(data);
    }

    render() {
        return (
            <div className="container py-4">
                <section className="card section">
                    <div className="container mx-3 mt-4">
                        <h3 className="text-dark font-weight-normal">Selamat datang di <span className="font-weight-bold">CoCreate Platform</span></h3>
                        <p className="text-muted">Ini tulisan suntuk subtitle nya. Terserah mau diisi apaan</p>
                    </div>
                    
                    <div className="row">
                        <div className="col-lg-6 col-12 order-lg-1 order-1 align-self-center">
                            <div className="p-4 m-3">
                                <Image src={img} width="100%"></Image>
                            </div>
                        </div>
    
                        <div className="col-lg-6 col-md-6 col-12 order-lg-2 order-2">
                            <div className="py-2 px-5 m-lg-4">
                                {/* <img src="../assets/img/stisla-fill.svg" alt="logo" width={80} className="shadow-light rounded-circle mb-5 mt-2" /> */}
                                <h4 className="text-dark font-weight-normal mb-5">Daftar</h4>
                                <form onSubmit={(event) => {
                                    this.handleSubmit(event);
                                }}
                                action="post"
                                >
                                    <div className="form-row">
                                        <div className="form-group col-md-6">
                                            <label htmlFor="first_name">Nama depan</label>
                                            <input 
                                                type="text"
                                                className="form-control"
                                                id="first_name"
                                                onChange={(event) => {
                                                    return (this.first_name =
                                                        event.target.value);
                                                }}
                                                required
                                            />
                                        </div>
                                        <div className="form-group col-md-6">
                                        <label htmlFor="last_name">Nama belakang</label>
                                            <input 
                                                type="text"
                                                className="form-control"
                                                id="last_name"
                                                onChange={(event) => {
                                                    return (this.last_name =
                                                        event.target.value);
                                                }}
                                                required
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email" className="control-label">Email</label>
                                        <input
                                            id="email"
                                            type="email"
                                            className="form-control"
                                            name="email"
                                            onChange={(event) => {
                                                return (this.email =
                                                    event.target.value);
                                            }}
                                            tabIndex={1}
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Mohon isi alamat email anda
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="d-block">
                                            <label htmlFor="password" className="control-label">Password</label>
                                        </div>
                                        <input
                                            id="password"
                                            type="password"
                                            className="form-control"
                                            name="password"
                                            onChange={(event) => {
                                                return (this.password =
                                                    event.target.value);
                                            }}
                                            tabIndex={2}
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Mohon isi password anda
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="d-block">
                                            <label htmlFor="confirm_password" className="control-label">Konfirmasi password</label>
                                        </div>
                                        <input
                                            id="confirm_password"
                                            type="password"
                                            className="form-control"
                                            name="confirm_password"
                                            onChange={(event) => {
                                                return (this.confirm_password =
                                                    event.target.value);
                                            }}
                                            tabIndex={2}
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Mohon ulangi password anda
                                        </div>
                                    </div>
                                    <div className="form-group text-right">
                                        {/* <a href="auth-forgot-password.html" className="float-right mt-3">
                                            Forgot Password?
                                        </a> */}
                                        <button type="submit" className="btn btn-primary btn-lg btn-icon icon-right">
                                            Daftar
                                        </button>
                                        <span className="float-left mt-3">
                                            Sudah punya akun? <a href="/login">Login</a>
                                        </span>
                                    </div>
                                    {/* <div className="mt-5 text-center">
                                        Belum punya akun? <a href="auth-register.html">Daftar sekarang</a>
                                    </div> */}
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </section>
            </div>
        )
    }
}
