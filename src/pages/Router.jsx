import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Registrasi from './auth/Registrasi'
import Login from './auth/Login'
import Home from './anggota/Home'
import EditProfil from './anggota/profil/Profil'

function Router(){
    return (
        <BrowserRouter>
            <Route exact path="/daftar" component={Registrasi}/>
            <Route exact path="/login" component={Login}/>
        </BrowserRouter>
    )
}

export default Router